import React, { Component } from 'react';

import './App.css';

var participants = [
   {  participantsName:'Jeewan Bhusal',participantsEmail: 'jeewanbhusal@gmail.com',participantsPhone:    '+358451638230'
   },{participantsName:'Jagjit singh',participantsEmail:  'jagjitsingh@gmail.com', participantsPhone:    '+358451638231'
   },{participantsName:'Sanjay thapa',participantsEmail:  'sanjaythapa@gmail.com',participantsPhone:     '+358451638232'
   },{participantsName:'Sachina paudel',participantsEmail:'sachinapaudel@gmail.com',participantsPhone:   '+358451638233'
   },{participantsName:'Shreeju kapoor',participantsEmail:'shreejukapoor@gmail.com',participantsPhone:   '+358451638234'
 },  {participantsName:'Sarita thapa',participantsEmail:  'saritathapa@gmail.com',participantsPhone:     '+358451638235'
   },{participantsName:'Suricha paudel',participantsEmail:'surichapaudel@gmail.com',participantsPhone:   '+358451638236'
 },{  participantsName:'sushmita subedi',participantsEmail:'sushmitasubedi@gmail.com',participantsPhone: '+358451638237'
   },{participantsName:'pukar shrestha',participantsEmail: 'pukarshrestha@gmail.com',participantsPhone:  '+358451638238'
 },{participantsName:  'sanjeev pariyar',participantsEmail:'sanjeevpariyar@gmail.com',participantsPhone: '+358451638239'
   },{participantsName:'ramesh paudel',participantsEmail:  'rameshpaudel@gmail.com',participantsPhone:   '+358451638240'
 },{participantsName:  'binay sapkota',participantsEmail:  'sapkotabinay@gmail.com',participantsPhone:   '+358451638241'
   },{participantsName:'tejendrasapkota',participantsEmail:'tejendrasapkota@gmail.com',participantsPhone:'+358451638242'
 },{participantsName:  'utsab sapkota',participantsEmail:  'sapkotautsab@gmail.com',participantsPhone:   '+358451638243'
   },{participantsName:'akshay kumar',participantsEmail:   'akshaykumar@gmail.com',participantsPhone:    '+358451638244'
 },{participantsName:  'ryan gosling',participantsEmail:   'ryangosling@gmail.com',participantsPhone:    '+358451638245'
   },{participantsName:'Chris hemsworth',participantsEmail:'chrishemsworth@gmail.com',participantsPhone: '+358451638246'
 },{participantsName:  'jennifer lawren',participantsEmail:'jenniferlawren@gmail.com',participantsPhone: '+358451638247'
   },{participantsName:'Emma stone',participantsEmail:     'emmastone@gmail.com',participantsPhone:      '+358451638248'
 },{participantsName:  'Aishwarya rai',participantsEmail:  'aishwaryarai@gmail.com',participantsPhone:   '+358451638249'
   }
]




class App extends Component {

   constructor(props){
     super(props);

  this.state={
    participants
  };

  this.handleAddParticipants = this.handleAddParticipants.bind(this);
}


  updateParticipants(index, newname, newemail, newphone){
    const users = this.state.participants;
    const user = participants[index];
    user['name'] = newname;
    user['email'] = newemail;
    user['phone'] = newphone;
    this.setState({
      users
    })
    }



   handleRemoveParticipant(index){
     this.setState({
       participants: this.state.participants.filter(function(e,i){
         return i !== index;
       })
     });
   }

   handleAddParticipants(participants){
     this.setState({participants: [...this.state.participants, participants]})
   }


  render() {
    const users = this.state.users
    return (
      <div className="container">
      <ParticipantInput onAddParticipants={this.handleAddParticipants}></ParticipantInput>


      <hr />
      <div id="data-table">
       <table className="table ">
         <thead>
          <tr className="distance">
            <th>Name</th>
            <th>E-mail Address</th>
            <th>Phone number</th>
          </tr>
       </thead>



        {this.state.participants.map((participant,index) =>
             <tbody key={index}>
                 <tr>

                    <td>{participant.participantsName}</td>
                    <td>{participant.participantsEmail}</td>
                    <td>{participant.participantsPhone}</td>
                    <td><button className="edit"><i className="fa fa-pencil" onClick = {this.updateParticipants.bind(this,index)} ></i></button></td>
                    <td><button className="delete" ><i className="fa fa-trash" onClick={this.handleRemoveParticipant.bind(this,index)} ></i></button></td>
                 </tr>

              </tbody>

        )}
      </table>


     </div>
      </div>

    );
  }
}


class ParticipantInput extends Component {
   constructor(props){
     super(props);

     this.state = {
       participantsName: '',
       participantsEmail: '',
       participantsPhone: ''
     }

     this.handleInputChange = this.handleInputChange.bind(this);
     this.handleSubmit = this.handleSubmit.bind(this);
  }

 handleInputChange(event){
   const target = event.target;
   const value = target.value;
   const name = target.name;

   this.setState({
     [name]: value
   });


}

handleSubmit(event){
  event.preventDefault();
  this.props.onAddParticipants(this.state);
  this.setState({
    participantsName:'',
    participantsEmail:'',
    participantsPhone:''
  });
}






     render() {
       return(
         <div>

            <form className="form-horizontal" onSubmit={this.handleSubmit} >
            <div className="form-group row">
             <div className="col-sm-3">
               <input name="participantsName"
                     type="text"
                     className="form-control"
                     id="inputparticipantsName"
                     value={this.state.participantsName}
                     onChange={this.handleInputChange}
                     placeholder="Full Name"

              ></input>

             </div>



              <div className="col-sm-3">
                <input name="participantsEmail"
                     type="text"
                     className="form-control"
                     id="inputparticipantsEmail"
                     value={this.state.participantsEmail}
                     onChange={this.handleInputChange}
                     placeholder="E-mail"

              ></input>


            </div>



            <div className="col-sm-3">
              <input name="participantsPhone"
                     type="text"
                     className="form-control"
                     id="inputparticipantsPhone"
                     value={this.state.participantsPhone}
                     onChange={this.handleInputChange}
                     placeholder="Phone"

              ></input>

            </div>



               <div className="col-sm-3">
                     <button type="submit" className="btn btn-success"  >Add New</button>
              </div>
            </div>

            </form>


         </div>


       );
     }

}





export default App;
